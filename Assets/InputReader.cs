using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text;
using Unity.VisualScripting;
using System;

public class InputReader : MonoBehaviour
{
    [Header("[------ REFERENCE ------]")]
	public TextMeshProUGUI TMP_DeviceInfo;
	public TextMeshProUGUI TMP_CurrentInput;
	public TextMeshProUGUI TMP_TemporaryLogText;

	// Face Buttons-------------------------
	public Image RI_Abutton;
    public Image RI_Bbutton;
    public Image RI_Xbutton;
	public Image RI_Ybutton;
		   
	public Image RI_StartButton;
	public Image RI_SelectButton;

	// Dpad Buttons-------------------------
	public Image RI_DpadUpButton;
	public Image RI_DpadDownButton;
	public Image RI_DpadLeftButton;
	public Image RI_DpadRightButton;

	// Sticks -------------------------
	public Image Img_Highlight_LeftStick;
	public TextMeshProUGUI TMP_Xaxis;
	public TextMeshProUGUI TMP_Yaxis;

	public Image Img_Highlight_RightStick;
	public TextMeshProUGUI TMP_FourthAxis;
	public TextMeshProUGUI TMP_FifthAxis;

	// Shoulder Buttons-------------------------
	public TextMeshProUGUI TMP_LeftBumper;
	public TextMeshProUGUI TMP_RightBumper;
	public TextMeshProUGUI TMP_ThirdAxis;
	public Slider Slider_ThirdAxis;
	public TextMeshProUGUI TMP_NinthAxis;
	public Slider Slider_NinthAxis;
	public TextMeshProUGUI TMP_TenthAxis;
	public Slider Slider_TenthAxis;


	[Header("[------ STATS ------]")]
    public float Duration_temporaryLogStringsVisible = 1f;
	public float Multiplier_StickMovement = 10f;
	public float CloseValue = 0.25f;

	public Color DownColor = Color.green;
	string downColorHex = string.Empty;
	public Color UpColor = Color.red;
	string upColorHex = string.Empty;
	public Color CloseColor = Color.red;
	string closeColorHex = string.Empty;

	public Color DownColor_axis = Color.green;
	string downColorHex_axis = string.Empty;
	public Color UpColor_axis = Color.red;
	string upColorHex_axis = string.Empty;

	[Header("[------ FLAGS ------]")]
	bool faceFlag = false;
	bool dpadFlag = false;
	bool shoulderFlag = false;
	bool stickFlag = false;


	//[Header("[------ OTHER ------]")]
	private List<string> temporaryLogStrings = new List<string>();
	private List<float> temporaryLogStringTimes = new List<float>();
	private Vector2 v_leftStickStartPos, v_rightStickStartPos;


	void Start()
    {
		TMP_DeviceInfo.text = GetConnectedJoystickNames();
		StartCoroutine( CheckForControllers() );
		TMP_TemporaryLogText.text = string.Empty;
        TMP_CurrentInput.text = string.Empty;

		v_leftStickStartPos = Img_Highlight_LeftStick.rectTransform.position;
		v_rightStickStartPos = Img_Highlight_RightStick.rectTransform.position;

		ResetFaceIndicators();
		ResetStickIndicators();
		ResetDpadIndicators();
		ResetShoulderIndicators();

		upColorHex = UnityEngine.ColorUtility.ToHtmlStringRGB(UpColor);
		downColorHex = UnityEngine.ColorUtility.ToHtmlStringRGB(DownColor);

		upColorHex_axis = UnityEngine.ColorUtility.ToHtmlStringRGB(UpColor_axis);
		downColorHex_axis = UnityEngine.ColorUtility.ToHtmlStringRGB(DownColor_axis);

		closeColorHex = UnityEngine.ColorUtility.ToHtmlStringRGB(CloseColor);
	}

	void Update()
    {
		if ( !string.IsNullOrEmpty(Input.inputString) )
        {
            AddTemporaryLogString( Input.inputString );
        }
		#region RESETS--------------------------////////////////////////////////
		TMP_CurrentInput.text = Input.inputString;

		if( faceFlag )
		{
			ResetFaceIndicators();
			faceFlag = false;
		}

		if ( shoulderFlag )
		{
			ResetShoulderIndicators();
			shoulderFlag = false;
		}

		if( stickFlag )
		{
			ResetStickIndicators();
		}

		if( dpadFlag )
		{
			ResetDpadIndicators();
			dpadFlag = false;
		}
		#endregion

		#region FACE INPUTS ---------------------/////////////////////////////////
		HandleMomentaryButtonInput( KeyCode.JoystickButton0, RI_Abutton );
		HandleMomentaryButtonInput( KeyCode.JoystickButton1, RI_Bbutton );
		HandleMomentaryButtonInput( KeyCode.JoystickButton2, RI_Xbutton );
		HandleMomentaryButtonInput( KeyCode.JoystickButton3, RI_Ybutton );
		HandleMomentaryButtonInput( KeyCode.JoystickButton6, RI_SelectButton );
		HandleMomentaryButtonInput( KeyCode.JoystickButton7, RI_StartButton );
		#endregion

		#region SHOULDER INPUTS---------------------------//////////////////////////
		HandleShoulderButtonInput( KeyCode.JoystickButton4, TMP_LeftBumper );
		HandleShoulderButtonInput( KeyCode.JoystickButton5, TMP_RightBumper );

		Slider_ThirdAxis.value = Input.GetAxis("Fire1");
		TMP_ThirdAxis.text = Input.GetAxis("Fire1").ToString("#.##");

		Slider_NinthAxis.value = Input.GetAxis("NinthAxis");
		TMP_NinthAxis.text = Input.GetAxis("NinthAxis").ToString("#.##");

		Slider_TenthAxis.value = Input.GetAxis("TenthAxis");
		TMP_TenthAxis.text = Input.GetAxis("TenthAxis").ToString("#.##");
		#endregion

		#region STICK INPUTS-----------------------//////////////////////////
		HandleStickVisual( Img_Highlight_LeftStick, v_leftStickStartPos, TMP_Xaxis, "Horizontal", TMP_Yaxis, "Vertical" );
		HandleStickVisual( Img_Highlight_RightStick, v_rightStickStartPos, TMP_FourthAxis, "RHorizontal", TMP_FifthAxis, "RVertical" );
		#endregion

		#region DPAD INPUTS-----------------/////////////////////////
		float dAxis = Input.GetAxis("DHorizontal");
		if ( dAxis < 0f )
		{
			RI_DpadLeftButton.enabled = true;
			dpadFlag = true;
		}
		else if ( dAxis > 0f )
		{
			RI_DpadRightButton.enabled = true;
			dpadFlag = true;
		}

		float vAxis = Input.GetAxis("DVertical");
		if ( vAxis < 0f )
		{
			RI_DpadUpButton.enabled = true;
			dpadFlag = true;
		}
		else if ( vAxis > 0f )
		{
			RI_DpadDownButton.enabled = true;
			dpadFlag = true;
		}
		#endregion

		if ( temporaryLogStrings != null && temporaryLogStrings.Count > 0 )
        {
            StringBuilder builtLogString = new StringBuilder();
            List<int> indicesToDelete = new List<int>();
            for ( int i = temporaryLogStrings.Count-1; i > -1; i-- )
            {
                temporaryLogStringTimes[i] -= Time.deltaTime;
                if ( temporaryLogStringTimes[i] < 0f )
                {
                    indicesToDelete.Add( i );

                }
                else
                {
                    builtLogString.Append( temporaryLogStrings[i] + Environment.NewLine );
                }
            }

            TMP_TemporaryLogText.text = builtLogString.ToString();

            if( indicesToDelete.Count > 0 )
            {
				int deletedCount = 0;
                for ( int i = 0; i < indicesToDelete.Count; i++ )
                {
                    //temporaryLogStrings.RemoveAt( i ); //These can cause an out of range exception
                    //temporaryLogStringTimes.RemoveAt( i );

					temporaryLogStrings.RemoveAt(i - deletedCount);
					temporaryLogStringTimes.RemoveAt(i - deletedCount);
					deletedCount++;
                }
            }
        }

		if( Event.current == null )
		{
			TMP_CurrentInput.text = "Null";

		}
		else if ( Event.current.keyCode != KeyCode.None )
		{
			TMP_CurrentInput.text = Event.current.keyCode.ToString();

		}
		else
		{
			TMP_CurrentInput.text = "None";
		}
    }

	private void HandleMomentaryButtonInput( KeyCode kc, Image img )
	{
		if ( Input.GetKeyDown(kc) )
		{
			AddTemporaryLogString( $"<color=#{downColorHex}>{kc} (down)-------------------</color>" );
		}
		else if ( Input.GetKey(kc) )
		{
			img.enabled = true;
			faceFlag = true;
			AddTemporaryLogString( $"{kc} (held)" );
		}
		else if ( Input.GetKeyUp(kc) )
		{
			AddTemporaryLogString( $"<color=#{upColorHex}>{kc} (up)</color>" );
		}
	}

	private void HandleShoulderButtonInput( KeyCode kc, TextMeshProUGUI tmp )
	{
		if ( Input.GetKeyDown(kc) )
		{
			AddTemporaryLogString($"<color=#{downColorHex}>{kc} (down)-------------------</color>");
		}
		else if ( Input.GetKey(kc) )
		{
			tmp.color = Color.yellow;
			shoulderFlag = true;
			AddTemporaryLogString( $"{kc} (held)" );
		}
		else if ( Input.GetKeyUp(kc) )
		{
			AddTemporaryLogString( $"<color=#{upColorHex}>{kc} (up)</color>" );
		}
	}

	private void HandleStickVisual( Image stickImg, Vector2 startPos, TextMeshProUGUI hTxtFld, string hAxisString, TextMeshProUGUI vTxtFld, string vAxisString )
	{
		float hAxisVal = Input.GetAxis( hAxisString );
		hTxtFld.text = hAxisVal.ToString("#.##");

		if ( hAxisVal >= 1f )
		{
			stickImg.color = Color.yellow;
			stickFlag = true;
		}
		else if (hAxisVal <= -1f)
		{
			stickImg.color = Color.yellow;
			stickFlag = true;
		}
		else if ( Math.Abs(hAxisVal) < CloseValue && hAxisVal != 0f )
		{
			Img_Highlight_LeftStick.color = CloseColor;
			stickFlag = true;
		}

		float vAxisVal = Input.GetAxis( vAxisString );
		vTxtFld.text = vAxisVal.ToString( "#.##" );
		if ( vAxisVal >= 1f )
		{
			stickImg.color = Color.yellow;
			stickFlag = true;
		}
		else if ( vAxisVal <= -1f )
		{
			stickImg.color = Color.yellow;
			stickFlag = true;
		}
		else if ( vAxisVal != 0f && Math.Abs(vAxisVal) < CloseValue )
		{
			stickImg.color = CloseColor;
			stickFlag = true;
		}

		stickImg.rectTransform.position = startPos + (Vector2.right * hAxisVal * Multiplier_StickMovement) +
			(Vector2.up * vAxisVal * Multiplier_StickMovement);
	}

	void OnGUI()
	{
		if ( Event.current.keyCode != KeyCode.None )
		{
			Debug.Log($"kc {Event.current.keyCode}");
		}
	}

	IEnumerator CheckForControllers()
	{
		while (true)
		{
			TMP_DeviceInfo.text = GetConnectedJoystickNames();

			yield return new WaitForSeconds(1f);
		}
	}

	string GetConnectedJoystickNames()
	{
		string[] names = Input.GetJoystickNames();
		return string.Join(Environment.NewLine, names);
	}

	#region RESET METHODS ------------------------//////////////////////////////
	void AddTemporaryLogString( string s_passed )
    {
        temporaryLogStrings.Add( s_passed );
        temporaryLogStringTimes.Add( Duration_temporaryLogStringsVisible );
    }

	private void ResetFaceIndicators()
	{
		RI_Abutton.enabled = false;
		RI_Bbutton.enabled = false;
		RI_Xbutton.enabled = false;
		RI_Ybutton.enabled = false;

		RI_StartButton.enabled = false;
		RI_SelectButton.enabled = false;

	}

	private void ResetShoulderIndicators()
	{
		TMP_LeftBumper.color = Color.white;
		TMP_RightBumper.color = Color.white;
	}

	private void ResetStickIndicators()
	{
		Img_Highlight_LeftStick.color = Color.white;
		Img_Highlight_RightStick.color = Color.white;

	}

	private void ResetDpadIndicators()
	{
		RI_DpadUpButton.enabled = false;
		RI_DpadDownButton.enabled = false;
		RI_DpadLeftButton.enabled = false;
		RI_DpadRightButton.enabled = false;
	}
	#endregion
}
